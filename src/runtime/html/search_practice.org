#+TITLE: Demonstration artefact for Insertion and Search in Tries
#+AUTHOR:AnimeshSinha
#+DATE:
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Features of the artefact
+ This aretefact implements the practice for searching in Tries
+ It prints out a word that the user needs to search for
+ As the user clicks the correct nodes they turn green one by one
+ Wrong clicks get colored in red and stay for 2 seconds
+ Once the end of word is reached, a new question will start off

* Setting up the the HTML and fetching all CSS dependencies
** Head Section Elements
Title, meta tags and all the links related to CSS of this
artefact comes under this section.  Calling Bootstrap and
Treant CSS, andjQuery
#+NAME: head-section-elems
#+BEGIN_SRC html
  <title>Tries and Suffix Tries</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">  
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/treant-js/1.0/Treant.css">
  <link href="../css/style_main.css" rel="stylesheet">
  <link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
#+END_SRC

** Instruction Box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instruction-box
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible">Instructions</button>
  <div class="content">
    <ul>
      <li>You can see a word at the bottom of the Screen saying <b>Search
          For</b>.</li>
      <li>Try searching for this word. Click the nodes as you would
        find them in search one by one.</li>
      <li>If you are correct, the node will go <b>GREEN</b>. If you are
        wrong, it will turn <b>RED</b></li>
      <li>If the node gets colored red, you get to retry in a second,
        try finding the right answer.</li>
      <li>If all nodes go green till the end, and you reach the $
        symbol, then the trie will clear for the next query.</li>
      <li>Repeat the exercise a few times till you get a hang of
        searching in tries.</li>
    </ul> 
  </div>
</div>

#+END_SRC

** Legend
This is a legend, that can referred to when observing.
#+NAME: legend
#+BEGIN_SRC html
<ul class="legend" style="padding-top:10px;">
  <li><span class="blue"></span> Unselected</li>
  <li><span class="green"></span> Selected and Correct</li>
  <li><span class="red"></span> Selected and Wrong</li>
</ul>

#+END_SRC

** Comments Box
This is a box in which the comments gets displayed, based on
what is happening currently in the artefact.
#+NAME: comments-box
#+BEGIN_SRC html
<div class="comment-box">
  <b>Search For&nbsp;</b>
  <p class="data"> </p>
</div>

#+END_SRC

** Artefact Section
Sets up the HTML for the following so that following
JavaScript can interact with the user through it:
+ Suffix Trie Rendering Block. (#render-5)
+ Show the user the text to be searched for (#bindings-3)
#+NAME: artefact-section
#+BEGIN_SRC html
<div class="row">
  <div class="chart" id="render-3"></div>
</div>

#+END_SRC

** Calling necessary JAVASCRIPT dependencies
Fetching jQuery, raphael, Treant.
#+NAME: js-libs
#+BEGIN_SRC html
<script
  src="https://code.jquery.com/jquery-3.4.0.min.js"
  integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.2.8/raphael.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/treant-js/1.0/Treant.min.js"></script>
<script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>

#+END_SRC

** Javascript for artefact
*** Fetching the JAVASCRIPT dependency from the associated artefacts directory
The Classes fetched are:
+ Trie (General Trie-Rendering class)
+ SearchPractice (Quiz the user on Searching and point out
  errors immediately)
#+NAME: main-js
#+BEGIN_SRC html
<script type="text/javascript" src="../js/tries_main.js"></script>
<script type="text/javascript" src="../js/search_pr.js"></script>

#+END_SRC

*** Declaration of the variables that initialize the trie
It defines the list of words that will be a part of the trie
and that the interface will ask search queries on.
#+NAME: js-event-handlers
#+BEGIN_SRC html
<script type="text/javascript">
var searchPractice = new SearchPractice("#render-3", "#bindings-3", [
    "Hallaloya", "Fried", "Fryams", "Friends", "Fervor", "Help"
]);
</script>

#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle search_practice.html :eval no :noweb yes
<!DOCTYPE HTML>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12">
              <<instruction-box>>
          </div>
      </div>
      <div class="row" style="margin-top: 21px;">
           <div class="col-sm-6">
              <<legend>>
           </div>
           <div class="col-sm-6">
              <<comments-box>>
           </div>
      </div>
      <<artefact-section>>
    </div>
    <<js-libs>>
    <<main-js>>
    <<js-event-handlers>>
  </body>
</html>
#+END_SRC

